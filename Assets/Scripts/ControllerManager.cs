﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerManager : MonoBehaviour
{


    //GameObjects;
    public Material[] SkyBoxes;
    private GameObject PlayerObj;




    //Delegates_______________________________________________________________________________________________
    public delegate void ControllerStateChangeHandler(controllerstate controllerstate);

    //Events__________________________________________________________________________________________________
    /// <summary>
    /// Occurs when controller state changed.
    /// </summary>
    public static event ControllerStateChangeHandler controllerStateChanged;

    //events_list_____________________________________________________________________________________________
    /// <summary>
    /// EventList for GameManager.
    /// </summary>
    public enum controllerstate { Enable, Disable, Reset };


    /// <summary>
    /// The _gs.
    /// </summary>
    private static controllerstate _gs;

    // Gets or Sets the VARIABLES
    /// <summary>
    /// Gets or sets the controllersState and call event for changed Controllerstate.
    /// </summary>
    /// <value>The gs.</value>
    public static controllerstate gs
    {
        get
        {
            return _gs;
        }
        set
        {
            _gs = value;
            if (controllerStateChanged != null)
            {
                controllerStateChanged(gs);
            }
        }
    }
    //variables

    /// <summary>
    ///Controls enabled or not;
    /// </summary>
    private static bool enable;
    public float jumpForce = 300;

    public float XVelocity = 600;
    public float worldSpeed = 100;
    public float forwardForce = 100;
    public float CameraDistance = 8;
    public bool waittostart;
    public float smoothRotation = 2.0F;
    public float tiltAngle = 30.0F;
    public static bool jump;
    public static bool DoubleJump;

    public static Transform playerLast;

    //functions

    void Awake()
    {
        controllerStateChanged += controllerStateChangedHandlerFunction;
        enable = false;
    }

    // Use this for initialization
    void Start()
    {
        waittostart = false;

    }

    void Update()
    {

       // MovePlayer();
        if (enable)
        {
            if (Input.GetMouseButtonDown(0))
            {

                if (jump)
                {

                    print("Jumping BABY");
                    PlayerObj.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
                    jump = false;
                }

            }

            if (PlayerObj.transform.position.y < -3)
            {
                GameManager.gs = GameManager.gamestate.Dead;
            }
        }
    }

    void FixedUpdate()
    {
        MovePlayer();
    }

    void MovePlayer()
    {
        if (enable)
        {
            Vector3 vel = PlayerObj.GetComponent<Rigidbody>().velocity;
            //print("VELOCITY:" + vel);
            if (vel.z <= 0.1)
            {
                //if (waittostart == true)
                  //  GameManager.gs = GameManager.gamestate.Dead;
            }
            waittostart = true;

#if UNITY_EDITOR


            PlayerObj.GetComponent<Rigidbody>().velocity = new Vector3(Input.GetAxis("Horizontal") * XVelocity * Time.deltaTime, vel.y, forwardForce);
            float tiltAroundZ = Input.GetAxis("Horizontal") * -tiltAngle;
            Quaternion target = Quaternion.Euler(0, 0, tiltAroundZ);
            Transform p = PlayerObj.transform.FindChild("MaPlaya2");
            //p.rotation = Quaternion.Slerp(p.rotation, target, Time.deltaTime * smoothRotation);


#elif UNITY_ANDROID

            
           
           
            PlayerObj.GetComponent<Rigidbody>().velocity = new Vector3(Input.acceleration.x * XVelocity * Time.deltaTime, vel.y, forwardForce);
            float tiltAroundZ = Input.acceleration.x * -tiltAngle;
            Quaternion target = Quaternion.Euler(0, 0, tiltAroundZ);
            Transform p= PlayerObj.transform.FindChild("MaPlaya2");
            //p.rotation = Quaternion.Slerp(p.rotation, target, Time.deltaTime * smoothRotation);
            

            
#endif
            p.rotation = Quaternion.LookRotation(PlayerObj.GetComponent<Rigidbody>().velocity);
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, PlayerObj.transform.position.z - CameraDistance);


        }
    }


    void controllerStateChangedHandlerFunction(controllerstate GS)
    {
        if (GS == controllerstate.Enable)
        {
            RenderSettings.skybox = SkyBoxes[Random.Range(0, SkyBoxes.Length)];
            PlayerObj = GameManager.getPlayerObj();
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, PlayerObj.transform.position.z - CameraDistance);

            waittostart = false;
            DoubleJump = true;
            jump = true;
            StartCoroutine(startDelay());
        }
        else if (GS == controllerstate.Disable)
        {

            enable = false;
        }
        else if (GS == controllerstate.Reset)
        {


        }


    }

    IEnumerator startDelay()
    {
        yield return new WaitForSeconds(1);
        enable = true;
    }

}
