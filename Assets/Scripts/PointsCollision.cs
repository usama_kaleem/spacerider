﻿using UnityEngine;
using System.Collections;

public class PointsCollision : MonoBehaviour {
    public void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            
            GuiManager.gs = GuiManager.guistate.AddScore;
            Destroy(this.gameObject);
        }
    }

}
