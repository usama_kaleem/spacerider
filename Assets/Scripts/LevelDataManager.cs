﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Xml;
using System.Xml.Serialization;


public class LevelDataManager : MonoBehaviour
{
    private static string [] mapData;

    public GameObject World;
    private static GameObject _World;
    public static Data info;

    private static int _setupLevel;

    public static int setupLevel
    {
        get
        {
            return _setupLevel;
        }
        set
        {
            _setupLevel = value;

            generateLevelFromFile();
        }
    }

    private static void generateLevelFromFile()
    {
        int x = 290;
        int z = 0;
  
        for (int i = 0; i < mapData.Length; i++)
        {
            for (int j = 0; j < mapData[i].Length; j++)
            {
                if (mapData[i][j].Equals('1'))
                {
                    GameObject o = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/" + "1", typeof(GameObject)), new Vector3(x, 0, z), Quaternion.identity);
                    o.transform.SetParent(_World.transform);
                    x++;
                }else if (mapData[i][j].Equals('.'))
                {
                    x++;
                }
            }
            x = 290;
            z++;
        }
    }

    
    private static void generateLevel()
    {
        foreach (Transform child in _World.transform)
        {
            Destroy(child.gameObject);
        }
        try
        {
            for (int i = 0; i < info.ObstacleName[setupLevel].Count; i++)
            {

                GameObject o = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/" + info.ObstacleName[setupLevel][i], typeof(GameObject)), new Vector3(info.obstacleX[setupLevel][i], info.obstacleY[setupLevel][i], info.obstacleZ[setupLevel][i]), Quaternion.identity);
                o.transform.SetParent(_World.transform);
            }
            for (int i = 0; i < info.PlatformName[setupLevel].Count; i++)
            {
                GameObject p = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/" + info.PlatformName[setupLevel][i], typeof(GameObject)), new Vector3(info.platformX[setupLevel][i], info.platformY[setupLevel][i], info.platformZ[setupLevel][i]), Quaternion.identity);
                p.transform.SetParent(_World.transform);
            }

            GameObject f = (GameObject)Instantiate((GameObject)Resources.Load("Prefabs/final", typeof(GameObject)), new Vector3(info.finalX[setupLevel], info.finalY[setupLevel], info.finalZ[setupLevel]), Quaternion.identity);
            f.transform.SetParent(_World.transform);
        }
        catch (Exception e)
        {
            print(e.Message);
        }
    }





    void Awake()
    {

        _World = World;
        info = new Data();

#if UNITY_EDITOR
        if (File.Exists(Application.streamingAssetsPath + "/MapFile/1.txt"))
        {
            mapData = File.ReadAllLines(Application.streamingAssetsPath + "/MapFile/1.txt");
           
                    
            

            
            
        }
        else
        {
            print("YOU SHIT");

        }


#elif UNITY_ANDROID
        

        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "MapFile/levelData.dat");
        StartCoroutine(androidFileLoad(filePath));
       


#endif

    }

    IEnumerator androidFileLoad(string path)
    {
        if (path.Contains("://"))
        {
            WWW www = new WWW(path);
            yield return www;
            
            


        }
        else
        {
            //WWW www = new WWW(path);
            //yield return www;
            // result = www.bytes;
            mapData = File.ReadAllLines(path);
            print(mapData);
           

        }
            
    }

}
