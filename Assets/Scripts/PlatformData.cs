﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;


public class PlatformData : MonoBehaviour
{

    public static Data info;
    public GameObject[] obstacles;
    public GameObject[] platform;
    public GameObject final;

    void Awake()
    {
        print(Application.streamingAssetsPath);
        info = new Data();

        if (File.Exists(Application.streamingAssetsPath + "/MapFile/levelData.dat"))
        {
            BinaryFormatter formater = new BinaryFormatter();
            //XmlSerializer serializer = new XmlSerializer(typeof(Data));
            FileStream file = File.Open(Application.streamingAssetsPath + "/MapFile/levelData.dat", FileMode.Open);
            //info = (Data)serializer.Deserialize(file);
            info = (Data)formater.Deserialize(file);
            file.Close();
        }
        else
        {
            info.ObstacleName = new List<List<string>>();
            info.PlatformName = new List<List<string>>();

            info.obstacleX = new List<List<float>>();
            info.obstacleY = new List<List<float>>();
            info.obstacleZ = new List<List<float>>();

            info.platformX = new List<List<float>>();
            info.platformY = new List<List<float>>();
            info.platformZ = new List<List<float>>();

            info.finalX = new List<float>();
            info.finalY = new List<float>();
            info.finalZ = new List<float>();

        }
    }
    // Use this for initialization
    void Start()
    {
        obstacles = GameObject.FindGameObjectsWithTag("obstacles");
        platform = GameObject.FindGameObjectsWithTag("platform");
        final = GameObject.FindGameObjectWithTag("final");
        print(final.transform.position.x.ToString());

        List<float> Ox = new List<float>();
        List<float> Oy = new List<float>();
        List<float> Oz = new List<float>();

        List<float> Px = new List<float>();
        List<float> Py = new List<float>();
        List<float> Pz = new List<float>();

        List<string> oName = new List<string>();
        List<string> pName = new List<string>();


        for (int i = 0; i < obstacles.Length; i++)
        {
            print(obstacles[i].GetComponent<orgPrefRet>().originalPrefab().name.Split(' ')[0]);
            oName.Add(obstacles[i].GetComponent<orgPrefRet>().originalPrefab().name.Split(' ')[0]);
            Ox.Add(obstacles[i].GetComponent<Transform>().transform.position.x);
            Oy.Add(obstacles[i].GetComponent<Transform>().transform.position.y);
            Oz.Add(obstacles[i].GetComponent<Transform>().transform.position.z);
        }

        for (int i = 0; i < platform.Length; i++)
        {
            pName.Add(platform[i].GetComponent<orgPrefRet>().originalPrefab().name.Split(' ')[0]);
            Px.Add(platform[i].GetComponent<Transform>().transform.position.x);
            Py.Add(platform[i].GetComponent<Transform>().transform.position.y);
            Pz.Add(platform[i].GetComponent<Transform>().transform.position.z);
        }


        info.ObstacleName.Add(oName);
        info.PlatformName.Add(pName);

        info.obstacleX.Add(Ox);
        info.obstacleY.Add(Oy);
        info.obstacleZ.Add(Oz);

        info.platformX.Add(Px);
        info.platformY.Add(Py);
        info.platformZ.Add(Pz);

        info.finalX.Add(final.transform.position.x);
        info.finalY.Add(final.transform.position.y);
        info.finalZ.Add(final.transform.position.z);

        save();


    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// creating for only level one
    /// </summary>

    void load()
    {

    }
    void save()
    {
        BinaryFormatter formater = new BinaryFormatter();
        //XmlSerializer serializer = new XmlSerializer(typeof(Data));
        if (File.Exists(Application.streamingAssetsPath + "/MapFile/levelData.dat"))
        {
            File.Delete(Application.streamingAssetsPath + "/MapFile/levelData.dat");
        }

        FileStream file = File.Create(Application.streamingAssetsPath + "/MapFile/levelData.dat");
        //serializer.Serialize(file, info);
        formater.Serialize(file, info);
        file.Close();
        print("SAVING FILE");
    }
}
//integer[level][objectnumber];
[Serializable]
public class Data
{
    public List<List<String>> ObstacleName;
    public List<List<String>> PlatformName;


    public List<List<float>> obstacleX;
    public List<List<float>> obstacleY;
    public List<List<float>> obstacleZ;

    public List<List<float>> platformX;
    public List<List<float>> platformY;
    public List<List<float>> platformZ;

    public List<float> finalX;
    public List<float> finalY;
    public List<float> finalZ;


}