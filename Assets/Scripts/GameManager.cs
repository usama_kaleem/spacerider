﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static int level;
    public GameObject explosion;
    public GameObject PlayerObjPref;
    public static GameObject PlayerObj;

    public GameObject SpawnPoint;


    private GameObject ExplosionInstance;
    //Delegates_______________________________________________________________________________________________
    public delegate void GameStateChangeHandler(gamestate gamestate);

    //Events__________________________________________________________________________________________________
    /// <summary>
    /// Occurs when game state changed.
    /// </summary>
    public static event GameStateChangeHandler gameStateChanged;

    //events_list_____________________________________________________________________________________________
    /// <summary>
    /// EventList for Manager.
    /// </summary>
    public enum gamestate { Start,Dead,Win };

    //VARIABLES_______________________________________________________________________________________________



    /// <summary>
    /// The _gs.
    /// </summary>
    private static gamestate _gs;

    // Gets or Sets the VARIABLES
    /// <summary>
    /// Gets or sets the gamesState and call event for changed Gamestate.
    /// </summary>
    /// <value>The gs.</value>
    public static gamestate gs
    {
        get
        {
            return _gs;
        }
        set
        {
            _gs = value;
            if (gameStateChanged != null)
            {
                gameStateChanged(gs);
            }
        }
    }

    void Awake()
    {

        gameStateChanged += gameStateChangedHandlerFunction;
    }
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static GameObject getPlayerObj()
    {
        return PlayerObj;
    }

    void gameStateChangedHandlerFunction(gamestate GS)
    {
        if (GS == gamestate.Start)
        {

            LevelDataManager.setupLevel = level;
            PlayerObj = Instantiate(PlayerObjPref);
            
            PlayerObj.transform.position=SpawnPoint.transform.position;
            ControllerManager.gs = ControllerManager.controllerstate.Enable;
        }else if (GS == gamestate.Dead)
        {
            ControllerManager.gs = ControllerManager.controllerstate.Disable;
            ControllerManager.gs = ControllerManager.controllerstate.Reset;
            ExplosionInstance = (GameObject) Instantiate(explosion, PlayerObj.transform.position, Quaternion.identity);
#if UNITY_ANDROID
            Handheld.Vibrate();
#endif
            Destroy(PlayerObj);
            StartCoroutine(WaitToDie());
           
        }else if(GS == gamestate.Win)
        {
            ControllerManager.gs = ControllerManager.controllerstate.Disable;
            ControllerManager.gs = ControllerManager.controllerstate.Reset;
            Destroy(PlayerObj);

            GuiManager.gs = GuiManager.guistate.EndMenuEnable;
            GuiManager.gs = GuiManager.guistate.endAnnouncementWin;
            
        }
    }
    IEnumerator WaitToDie()
    {
        yield return new WaitForSeconds(2);
        Destroy(ExplosionInstance);
        GuiManager.gs = GuiManager.guistate.EndMenuEnable;
    }

    
}
