﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controller : MonoBehaviour {

    public GameObject[] PlatformsPref;
    public GameObject[] ObstaclesPref;
    public Transform worldLimit;
    public Transform platformSpawnPoint;

    //GameObjects;

    private GameObject PlayerObj;
    
  
    public Transform SpawnPoint;


    private List<GameObject> Platforms;
    private List<GameObject> Obstacles;


    //Delegates_______________________________________________________________________________________________
    public delegate void ControllerStateChangeHandler(controllerstate controllerstate);

    //Events__________________________________________________________________________________________________
    /// <summary>
    /// Occurs when controller state changed.
    /// </summary>
    public static event ControllerStateChangeHandler controllerStateChanged;

    //events_list_____________________________________________________________________________________________
    /// <summary>
    /// EventList for GameManager.
    /// </summary>
    public enum controllerstate { Enable,Disable,Reset };


    /// <summary>
    /// The _gs.
    /// </summary>
    private static controllerstate _gs;

    // Gets or Sets the VARIABLES
    /// <summary>
    /// Gets or sets the controllersState and call event for changed Controllerstate.
    /// </summary>
    /// <value>The gs.</value>
    public static controllerstate gs
    {
        get
        {
            return _gs;
        }
        set
        {
            _gs = value;
            if (controllerStateChanged != null)
            {
                controllerStateChanged(gs);
            }
        }
    }
    //variables

    /// <summary>
    ///Controls enabled or not;
    /// </summary>
    private static bool enable;
    public float jumpForce=300;
    public float velocity=10;
    public float worldSpeed = 100;



    //functions

    void Awake()
    {
        controllerStateChanged += controllerStateChangedHandlerFunction;
        enable = false;
    }

    // Use this for initialization
    void Start () {
        Platforms = new List<GameObject>();
        Obstacles = new List<GameObject>();


    }
	
	// Update is called once per frame
	void Update () {
        Terrain();
        obstacles();
        movePlayer();
       
        if(enable)
        if (PlayerObj.transform.position.y < 0.5)
        {
            GameManager.gs = GameManager.gamestate.Dead;
        }
    }
    void movePlayer()
    {
        if (enable)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (PlayerObj.transform.position.y < 1.2 && PlayerObj.transform.position.y > 0.8)
                {
                    PlayerObj.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
                }
            }
            PlayerObj.GetComponent<Transform>().transform.Translate(new Vector3(Input.acceleration.x *Time.deltaTime * velocity, 0, 0));

            //Restriction on movement;
            if (PlayerObj.GetComponent<Transform>().transform.position.x > 291 + 5)
                PlayerObj.GetComponent<Transform>().transform.position = new Vector3(291 + 5, PlayerObj.GetComponent<Transform>().transform.position.y, 16); // can't go further than 5
            else if (PlayerObj.GetComponent<Transform>().transform.position.x < 291 - 5)
                PlayerObj.GetComponent<Transform>().transform.position = new Vector3(291 - 5, PlayerObj.GetComponent<Transform>().transform.position.y, 16); // can't go further than -5



        }
        else
        {


        }
    }


    void Terrain()
    {
        if (enable)
        {

            if (Platforms.Count == 0)
            {
                Platforms.Add(Instantiate(PlatformsPref[0]));

                Platforms[Platforms.Count - 1].transform.position = platformSpawnPoint.position;

            }
            for (int i = 0; i < Platforms.Count; i++)
            {
                Platforms[i].transform.Translate(Vector3.back * Time.deltaTime * worldSpeed);
            }
            if (Platforms[Platforms.Count - 1].transform.position.z < worldLimit.position.z)
            {
                Platforms.Add(Instantiate(PlatformsPref[Random.Range(0, PlatformsPref.Length)]));
                Platforms[Platforms.Count - 1].transform.position = new Vector3(platformSpawnPoint.position.x, platformSpawnPoint.position.y, platformSpawnPoint.position.z + Random.Range(40, 50));
            }
            if (Platforms[0].transform.position.z < -14)
            {
                Destroy(Platforms[0]);
                Platforms.RemoveAt(0);
            }
        }


    }

    void obstacles()
    {
        if (enable)
        {
            if (Obstacles.Count == 0)
            {
                Obstacles.Add(Instantiate(ObstaclesPref[Random.Range(0, ObstaclesPref.Length)]));
                Obstacles[Obstacles.Count - 1].transform.position = new Vector3(Random.Range(SpawnPoint.position.x - 5, SpawnPoint.position.x + 5), SpawnPoint.position.y, Random.Range(SpawnPoint.position.z + 10, SpawnPoint.position.z + 30));
            }
            for (int i = 0; i < Obstacles.Count; i++)
            {
                Obstacles[i].transform.Translate(Vector3.back * Time.deltaTime * worldSpeed);
            }
            if (Obstacles[Obstacles.Count - 1].transform.position.z < worldLimit.position.z)
            {

                for (int i = 0; i < Random.Range(1, 4); i++)
                {
                    Obstacles.Add(Instantiate(ObstaclesPref[Random.Range(0, ObstaclesPref.Length)]));
                    Obstacles[Obstacles.Count - 1].transform.position = new Vector3(Random.Range(SpawnPoint.position.x - 5, SpawnPoint.position.x + 5), SpawnPoint.position.y, Random.Range(SpawnPoint.position.z + 10, SpawnPoint.position.z + 30));
                }
            }
            if (Obstacles[0].transform.position.z < 15)
            {
                GuiManager.gs = GuiManager.guistate.AddScore;
                Destroy(Obstacles[0]);
                Obstacles.RemoveAt(0);
            }


        }
    }


    void controllerStateChangedHandlerFunction(controllerstate GS)
    {
        if (GS == controllerstate.Enable)
        {
            PlayerObj = GameManager.getPlayerObj();
            enable = true;
        }
        else if (GS == controllerstate.Disable)
        {
            enable = false;
        }
        else if (GS == controllerstate.Reset)
        {
            while (Obstacles.Count != 0)
            {
                Destroy(Obstacles[0]);
                Obstacles.RemoveAt(0);
            }
            while (Platforms.Count != 0)
            {
                Destroy(Platforms[0]);
                Platforms.RemoveAt(0);
            }

        }


    }

}
