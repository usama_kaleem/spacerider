﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour
{
    //Menus
    public GameObject StartMenu;
    public GameObject blind;
    public GameObject EndMenu;
    public GameObject InGameMenu;
    public GameObject Score;
    public GameObject FinalScore;
    public GameObject LevelMenu;
    public GameObject SettingsMenu;
    public GameObject endAnnouncement;


    public GameObject levelButton;
    public GameObject LevelPanel;

    int score=0;






    //Delegates_______________________________________________________________________________________________
    public delegate void guiStateChangeHandler(guistate guistate);

    //Events__________________________________________________________________________________________________
    /// <summary>
    /// Occurs when gui state changed.
    /// </summary>
    public static event guiStateChangeHandler guiStateChanged;

    //events_list_____________________________________________________________________________________________
    /// <summary>
    /// EventList for GameManager.
    /// </summary>
    public enum guistate { DisableAll, StartMenuEnable, StartMenuDisable, BlindEnable, BlindDisable, EndMenuEnable, EndMenuDisable,InGameMenuEnable, InGameMenuDisable, AddScore ,LevelMenuEnable,LevelMenuDisable,endAnnouncementWin,SettingsMenuEnable, SettingsMenuDisable };

    //VARIABLES_______________________________________________________________________________________________



    /// <summary>
    /// The _gs.
    /// </summary>
    private static guistate _gs;

    // Gets or Sets the VARIABLES
    /// <summary>
    /// Gets or sets the guisState and call event for changed guistate.
    /// </summary>
    /// <value>The gs.</value>
    public static guistate gs
    {
        get
        {
            return _gs;
        }
        set
        {
            _gs = value;
            if (guiStateChanged != null)
            {
                guiStateChanged(gs);
            }
        }
    }

    /// <summary>
    /// Player presses Start to Play the game;
    /// </summary>
    public void onStartClicked()
    {
        gs = guistate.DisableAll;
        GameManager.gs = GameManager.gamestate.Start;
     

    }

    public void onSettingsBackButtonPressed()
    {
        gs = guistate.DisableAll;
        gs = guistate.BlindEnable;
        gs = guistate.StartMenuEnable;
    }

    public void onRestartClicked()
    {
        gs = guistate.DisableAll;
        GameManager.gs = GameManager.gamestate.Start;
        gs = guistate.InGameMenuEnable;
        Score.GetComponent<Text>().text = "0";
        score = 0;
    }

    public void onLevelButtonPressed()
    {
        gs = guistate.LevelMenuEnable;
    }

    public void onSettingsButtonClicked()
    {
        gs = guistate.DisableAll;
        gs = guistate.SettingsMenuEnable;

    }

    public void YSensitivityChanged(float val)
    {
        GetComponent<ControllerManager>().XVelocity = val;
    }

    void Awake()
    {
        guiStateChanged += guiStateChangedHandlerFunction;
    }

    // Use this for initialization
    void Start()
    {
        gs = guistate.DisableAll;
        gs = guistate.BlindEnable;
        gs = guistate.StartMenuEnable;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void guiStateChangedHandlerFunction(guistate GS)
    {
        if (GS == guistate.DisableAll)
        {
            LevelMenu.SetActive(false);
            InGameMenu.SetActive(false);
            blind.SetActive(false);
            StartMenu.SetActive(false);
            EndMenu.SetActive(false);
            SettingsMenu.SetActive(false);
        }
        else if (GS == guistate.StartMenuEnable)
        {
            blind.SetActive(true);
            StartMenu.SetActive(true);
        }
        else if (GS == guistate.StartMenuDisable)
        {
            blind.SetActive(false);
            StartMenu.SetActive(false);
        }
        else if (GS == guistate.BlindEnable)
        {
            blind.SetActive(true);
        }
        else if (GS == guistate.BlindDisable)
        {
            blind.SetActive(false);
        }
        else if (GS == guistate.EndMenuEnable)
        {
            gs = guistate.DisableAll;
            blind.SetActive(true);
            EndMenu.SetActive(true);
            FinalScore.GetComponent<Text>().text = score.ToString();
        }
        else if (GS == guistate.EndMenuDisable)
        {
            blind.SetActive(false);
            EndMenu.SetActive(false);
        }
        else if (GS == guistate.InGameMenuEnable)
        {
            gs = guistate.DisableAll;
            
            InGameMenu.SetActive(true);
        }
        else if (GS == guistate.InGameMenuDisable)
        {
            InGameMenu.SetActive(false);
        }
        else if (GS == guistate.AddScore)
        {
            score = int.Parse(Score.GetComponent<Text>().text) + 1;
            Score.GetComponent<Text>().text = score.ToString();
        }
        else if (GS == guistate.LevelMenuEnable)
        {
            gs = guistate.DisableAll;
            blind.SetActive(true);
            LevelMenu.SetActive(true);
            GameObject[] lvl = GameObject.FindGameObjectsWithTag("levelButton");
            for (int i = 0; i < lvl.Length; i++)
            {
                Destroy(lvl[i]);
            }
            for (int i = 0; i < LevelDataManager.info.ObstacleName.Count; i++)
            {
                GameObject myButton = Instantiate(levelButton) as GameObject;
                myButton.name = i.ToString();
                myButton.GetComponent<Transform>().SetParent(LevelPanel.transform);
                myButton.GetComponentInChildren<Text>().text = (i + 1).ToString();
                myButton.tag = "levelButton";
                myButton.GetComponent<Button>().onClick.AddListener(() =>
                {
                    LevelButtonPressed(myButton);
                });
            }
        }
        else if(GS== guistate.LevelMenuDisable)
        {
            blind.SetActive(false);
            LevelMenu.SetActive(false);
        }else if(GS== guistate.endAnnouncementWin)
        {
            endAnnouncement.GetComponent<Text>().text = "Level Cleared";
        }else if(GS== guistate.SettingsMenuEnable)
        {
            GS = guistate.DisableAll;
            blind.SetActive(true);
            SettingsMenu.SetActive(true);
        }else if(GS== guistate.SettingsMenuDisable)
        {
            SettingsMenu.SetActive(false);
            blind.SetActive(false);
        }
    }
    void LevelButtonPressed(GameObject myButton)
    {
        gs = guistate.LevelMenuDisable;
        
        GameManager.level = int.Parse(myButton.name);

        GameManager.gs = GameManager.gamestate.Start;
    }
}
